<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {

	public function index()
	{
		$data = [
			'title' => "Data Keuangan",
			"keuangan" => $this->db->get('keuangan')->result_array()
		];
		$this->load->view('keuangan/index', $data);
	}

	public function tambah()
	{
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|numeric');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => "Tambah Data Keuangan",
			];
			$this->load->view('keuangan/tambah', $data);		
		} else {
			$data = [
				'jumlah' => $this->input->post('jumlah'),
				'deskripsi' => $this->input->post('deskripsi')
			];
			$this->db->insert('keuangan', $data);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil ditambahkan!</div>');
			redirect(base_url('keuangan/index'));
		}

	}

	public function ubah($id)
	{
		$this->form_validation->set_rules('jumlah', 'Jumlah', 'required|numeric');
		$this->form_validation->set_rules('deskripsi', 'Deskripsi', 'required');

		if ($this->form_validation->run() == FALSE) {
			$data = [
				'title' => "Ubah Data Keuangan",
				'data' => $this->db->get_where('keuangan', ['id' => $id])->row_array()
			];
			$this->load->view('keuangan/ubah', $data);		
		} else {
			$data = [
				'jumlah' => $this->input->post('jumlah'),
				'deskripsi' => $this->input->post('deskripsi')
			];
			$this->db->update('keuangan', $data, ['id' => $id]);
			$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil diubah!</div>');
			redirect(base_url('keuangan/index'));
		}
	}

	public function hapus($id)
	{
		$this->db->delete('keuangan', ['id' => $id]);
		$this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Data berhasil dihapus!</div>');
		redirect(base_url('keuangan/index'));
	}
}
