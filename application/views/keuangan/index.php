<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <title><?= $title; ?></title>
  </head>
  <body>
    <div class="container">
        <div class="row mt-3">
            <div class="col-12">
                <h1>Data Keuangan</h1><br>
                <a href="<?= base_url('keuangan/tambah') ?>" class="btn btn-primary">Tambah Data</a><br><br>
                <?= $this->session->flashdata('message'); ?>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">#</th>
                        <th scope="col">Jumlah Uang</th>
                        <th scope="col">Deskripsi</th>
                        <th scope="col">Kapan</th>
                        <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $nomer = 1; ?>
                        <?php foreach ($keuangan as $row) : ?>
                        <tr>
                        <th scope="row"><?= $nomer; ?></th>
                        <td><?= $row['jumlah'] ?></td>
                        <td><?= $row['deskripsi'] ?></td>
                        <td><?= $row['is_created'] ?></td>
                        <td>
                            <a class="badge badge-info" href="<?= base_url('keuangan/ubah/'.$row['id']) ?>">ubah</a>
                            <a class="badge badge-danger" href="<?= base_url('keuangan/hapus/'.$row['id']) ?>" onclick="return confirm('Apakah Anda Yakin?')">hapus</a>
                        </td>
                        </tr>
                        <?php $nomer = $nomer + 1 ?>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  </body>
</html>